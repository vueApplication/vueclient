import Posts from '@/components/pages/PostsPage';
import HelloWorld from '@/components/HelloWorld';
import HelloUser from '@/components/HelloUser';
import authForm from '@/components/auth/authForm';

const routes = [{
  path: '/',
  name: 'Start',
  component: HelloWorld
},
{
  path: '/posts',
  name: 'Posts',
  component: Posts
},
{
  path: '/user/:id',
  name: 'HelloUser',
  component: HelloUser
},
{
  path: '/login',
  name: 'login',
  component: authForm
}];

export default routes;
